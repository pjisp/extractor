#!/bin/bash

# Dependencies: sudo apt-get install parallel

tar xf sve*.tar
ls provera* | parallel tar xf {} --wildcards '*.c'
rm provera*.tgz
mkdir $1 
ls ./home/provera | grep 'ee' | parallel mv ./home/provera/{}/*.c ./$1/{}.c
rm -r home
